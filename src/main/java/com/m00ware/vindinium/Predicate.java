package com.m00ware.vindinium;

public interface Predicate<T> {
	public boolean apply(T o);
}
