package vindinium;

/**
 * Play direction.
 */
public enum Direction {
    STAY("Stay", 0, 0),
    NORTH("North", -1, 0),
    SOUTH("South", 1, 0),
    EAST("East", 0, 1),
    WEST("West", 0, -1);

    // --- Properties ---

    /**
     * String representation
     */
    public final String name;
    public final int dx, dy;

    private Direction(final String name, int dx, int dy) {
        this.name = name;
        this.dx = dx;
        this.dy = dy;
    }

    /**
     * Returns string representation/name.
     */
    public String toString() {
        return this.name;
    } // end of toString
} // end of enumeration Direction

