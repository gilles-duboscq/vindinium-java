package vindinium;

import vindinium.Board.Position;

/**
 * Hero/game player.
 */
public final class Hero {
    public final int id;
    public final String name;
    public final Position spawnPosition;
    public final int elo;
    public final boolean crashed;
    public final HeroState state;
    private String userId;

    public static final class HeroState {
        public final int life;
        public final int gold;
        public final int mines;
        public final Position position;

        public HeroState(int life, int gold, int mines, Position position) {
            this.life = life;
            this.gold = gold;
            this.mines = mines;
            this.position = position;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + gold;
            result = prime * result + life;
            result = prime * result + mines;
            result = prime * result + ((position == null) ? 0 : position.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof HeroState)) {
                return false;
            }
            HeroState other = (HeroState) obj;
            if (gold != other.gold) {
                return false;
            }
            if (life != other.life) {
                return false;
            }
            if (mines != other.mines) {
                return false;
            }
            if (position == null) {
                if (other.position != null) {
                    return false;
                }
            } else if (!position.equals(other.position)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "HeroState [life=" + life + ", gold=" + gold + ", mines=" + mines + ", position=" + position + "]";
        }
    }

    public Hero(int id, String name, String userId, Position spawnPosition, int elo, boolean crashed, HeroState state) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.spawnPosition = spawnPosition;
        this.state = state;
        this.elo = elo;
        this.crashed = crashed;
    }

    // --- Properties ---

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (crashed ? 1231 : 1237);
        result = prime * result + elo;
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result + ((spawnPosition == null) ? 0 : spawnPosition.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Hero)) {
            return false;
        }
        Hero other = (Hero) obj;
        if (crashed != other.crashed) {
            return false;
        }
        if (elo != other.elo) {
            return false;
        }
        if (id != other.id) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (userId == null) {
            if (other.userId != null) {
                return false;
            }
        } else if (!userId.equals(other.userId)) {
            return false;
        }
        if (spawnPosition == null) {
            if (other.spawnPosition != null) {
                return false;
            }
        } else if (!spawnPosition.equals(other.spawnPosition)) {
            return false;
        }
        if (state == null) {
            if (other.state != null) {
                return false;
            }
        } else if (!state.equals(other.state)) {
            return false;
        }
        return true;
    }
}
