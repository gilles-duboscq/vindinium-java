package vindinium;

import org.apache.commons.lang3.builder.*;

/**
 * Game board.
 */
public final class Board {
    // --- Properties ---

    /**
     * Size (width or height, expect same for both)
     */
    private final int size;

    /**
     * Block/tiles
     */
    private final Tile[/* x */][/* y */] tiles;

    private final int[] mines;

    // --- Constructors ---

    /**
     * Bulk constructor.
     */
    public Board(final Tile[][] tiles) {
        if (tiles == null || tiles.length == 0) {
            throw new IllegalArgumentException();
        } // end of if

        // ---

        this.size = tiles[0].length;
        this.tiles = tiles;
        int maxHero = 0;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                Tile tile = tileAt(new Position(x, y));
                if (tile instanceof HeroTile) {
                    HeroTile heroTile = (HeroTile) tile;
                    if (heroTile.hero > maxHero) {
                        maxHero = heroTile.hero;
                    }
                }
            }
        }
        this.mines = new int[maxHero];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                Tile tile = tileAt(new Position(x, y));
                if (tile instanceof Mine) {
                    Mine mine = (Mine) tile;
                    if (mine.isOwned()) {
                        this.mines[mine.hero - 1]++;
                    }
                }
            }
        }
    } // end of <init>

    // --- Object support ---

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Board)) {
            return false;
        } // end of if

        final Board other = (Board) o;

        return new EqualsBuilder().append(this.size, other.size).append(this.tiles, other.tiles).isEquals();

    } // end of equals

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return new HashCodeBuilder(1, 3).append(this.size).append(this.tiles).toHashCode();

    } // end of hashCode

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return new ToStringBuilder(this).append("size", this.size).append("tiles", this.tiles).toString();

    } // end of toString

    // ---

    /**
     * Board block/tile.
     */
    public static class Tile {
        // --- Well-know instances ---

        /**
         * Air/empty block
         */
        public static final Tile AIR = new Tile("  ");

        /**
         * Free mine (not owned)
         */
        public static final Tile FREE_MINE = new Mine(-1);

        public static final Tile TAVERN = new Tile("[]");
        public static final Tile WALL = new Tile("##");

        // --- Properties ---

        /**
         * String representation
         */
        private final String repr;

        // --- Constructors ---

        /**
         * Constructor from string |representation|.
         */
        private Tile(final String representation) {
            this.repr = representation;
        } // end of <init>

        /**
         * Returns tiles matching given |representation|.
         * 
         * @throws IllegalArgumentException
         *             if representation is not supported
         */
        public static Tile of(final String representation) {
            if ("  ".equals(representation)) {
                return AIR;
            } else if ("##".equals(representation)) {
                return WALL;
            } else if ("[]".equals(representation)) {
                return TAVERN;
            } else if ("$-".equals(representation)) {
                return FREE_MINE;
            } else if (representation.startsWith("$")) {
                try {
                    return new Mine(Integer.parseInt(representation.substring(1)));
                } catch (Exception e) {
                    throw new IllegalArgumentException("Invalid mine tile: " + representation.substring(1));

                } // end of catch
            } else if (representation.startsWith("@")) {
                try {
                    return new HeroTile(Integer.parseInt(representation.substring(1)));
                } catch (Exception e) {
                    throw new IllegalArgumentException("Invalid hero tile: " + representation.substring(1));

                } // end of catch
            } // end of else if

            throw new IllegalArgumentException("Unsupported tile: " + representation);

        } // end of valueOf

        // --- Object support ---

        /**
         * {@inheritDoc}
         */
        public String toString() {
            return this.repr;
        }

        /**
         * {@inheritDoc}
         */
        public boolean equals(Object o) {
            if (o == null || !(o instanceof Tile)) {
                return false;
            } // end of if

            final Tile other = (Tile) o;

            return ((this.repr == null && other.repr == null) || (this.repr.equals(other.repr)));

        } // end of equals

        /**
         * {@inheritDoc}
         */
        public int hashCode() {
            return this.repr.hashCode();
        } // end of hashCode
    } // end of class Tile

    public static class Mine extends Tile {
        private final int hero;

        public Mine(final int hero) {
            super("$" + (hero < 0 ? "-" : hero));
            this.hero = hero;
        }

        public boolean isOwned() {
            return hero >= 0;
        }

        public boolean isOwnedBy(Hero player) {
            return player.id == hero;
        }
    }

    public static class HeroTile extends Tile {
        private final int hero;

        public HeroTile(final int hero) {
            super("@" + hero);
            if (hero < 0)
                throw new IllegalArgumentException();
            this.hero = hero;
        }

        public boolean is(Hero player) {
            return player.id == hero;
        }

        public int getHeroId() {
            return hero;
        }
    }

    public static final class Position {
        @Override
        public String toString() {
            return "Position [x=" + x + ", y=" + y + "]";
        }

        public final int x;
        public final int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean isValid(Board b) {
            return x >= 0 && y >= 0 && x < b.size && y < b.size;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + x;
            result = prime * result + y;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (!(obj instanceof Position)) {
                return false;
            }
            Position other = (Position) obj;
            if (x != other.x) {
                return false;
            }
            if (y != other.y) {
                return false;
            }
            return true;
        }

        public static Position of(int x, int y) {
            return new Position(x, y);
        }

    }

    public int getSize() {
        return size;
    }

    public Tile tileAt(Position p) {
        return tiles[p.x][p.y];
    }

    public int minesForHero(Hero h) {
        return this.mines[h.id - 1];
    }
} // end of class Board
